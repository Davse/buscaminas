var document = document || {}, 
    matriz= new Array(), 
    arrayBombas= new Array(),
    cronometro,
    cantBombas = 8,
    win=0,
    Buscaminas = {
    ancho: 10,
    alto: 10,
    iniciar: function(contenedor){
        var tipo = contenedor.substr(0,1),
            eContenedor,
            varcontenedor = contenedor.substr(1,contenedor.length);
            if (tipo == "."){
            eContenedor = document.getElementsByClassName(varcontenedor)[0];
        } else {
            eContenedor = document.getElementById(varcontenedor);
        }
        eContenedor.setAttribute("class", eContenedor.getAttribute("class")+" pawBuscaminas");
        eContenedor.appendChild(this.generarPanelDeControl());
        eContenedor.appendChild(this.generarCampoDeMinas());
    },
    

        
    abrirCeros: function(x,y){
        var objdiv;
        for (var dx = -1; dx <= 1; dx++) {
            for (var dy = -1; dy <= 1; dy++) {
                objdiv = document.getElementById((x + dx)+''+(y + dy));
                var controlaCoordenada = (((x + dx) >= 0) && ((x + dx) < Buscaminas.alto) && ((y + dy) >= 0) && ((y + dy) < Buscaminas.ancho));
              if ( controlaCoordenada && (matriz[x + dx][y + dy] == 0) && objdiv.getAttribute("class") =="oculto"){
                objdiv.classList.remove("oculto");
                objdiv.classList.add("visibleVacio");
                objdiv.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});  
                Buscaminas.abrirCeros((x + dx),(y + dy));
                win++;
                Buscaminas.verificarWin(win);  
              }else{
                    if(controlaCoordenada && matriz[x + dx][y + dy]>0 && objdiv.getAttribute("class") =="oculto"){
                         objdiv.innerHTML = matriz[x + dx][y + dy];
                         objdiv.classList.remove("oculto");
                         objdiv.classList.add("visibleNumero");    
                         objdiv.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});
                         win++;
                        Buscaminas.verificarWin(win);
                    }
                }
            }
          }
    }, 
        
    gameOver: function(){
            var objdiv,eFondo,eBoton,eTexto;
        Buscaminas.detenerTiempo();
        for (var x = 0; x < Buscaminas.alto; x++) {
            for (var y = 0; y < Buscaminas.ancho; y++) {
                objdiv = document.getElementById(x+''+y);
                var controlaVista = (objdiv.getAttribute("class") == "oculto");
              if ( controlaVista && (matriz[x][y] == 0)){
                objdiv.classList.remove("oculto");
                objdiv.classList.add("visibleVacio");
                objdiv.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});  
              }else{
                    if(controlaVista && matriz[x][y]>0){
                         objdiv.innerHTML = matriz[x][y];
                         objdiv.classList.remove("oculto");
                         objdiv.classList.add("visibleNumero");
                         objdiv.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});
                    }else{
                        if(controlaVista){
                         objdiv.classList.remove("oculto");    
                         objdiv.classList.add("visibleBombaDisable");
                         objdiv.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});
                        }
                    }
                }
            }
          }
        Buscaminas.includeCSS();
        eFondo=document.createElement("div");
        eBoton=document.createElement("div");
        eTexto=document.createElement("p");
        eFondo.setAttribute("class","gameoverF");
        eBoton.setAttribute("class","gameoverB");
        eBoton.addEventListener("click",function(){ location.reload(true);});
        eTexto.setAttribute("id","gameoverT");
        eTexto.innerHTML='GAME OVER';
        eBoton.appendChild(eTexto);
        eFondo.appendChild(eBoton);
        document.getElementsByTagName("body")[0].appendChild(eFondo);
    },
        
    cambiarEstadoDeCelda: function(event){
        var coordenadas = event.target.getAttribute("id").split(""), f=parseInt(coordenadas[0],10), c=parseInt(coordenadas[1],10);
        event.target.classList.remove("oculto");
        if(matriz[f][c] == 0){
            event.target.classList.add("visibleVacio");
            event.target.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});
            Buscaminas.abrirCeros(f,c);
            win++;
            Buscaminas.verificarWin(win);
           }else if(matriz[f][c] > 0){
                    event.target.innerHTML = matriz[f][c];
                    event.target.classList.add("visibleNumero");
                    event.target.addEventListener("click", function(){this.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda)});
                    win++;
                    Buscaminas.verificarWin(win);
                    }else{
                        event.target.classList.add("visibleBomba");
                        Buscaminas.gameOver();
                    }
    },
        
    toogleBloqueada: function(event){
        if(event.target.getAttribute("class")=="oculto"){
            event.target.classList.add("blokeada");
            event.target.removeEventListener("click", Buscaminas.cambiarEstadoDeCelda);
            document.getElementById("bombas").innerHTML = document.getElementById("bombas").innerHTML -1;
        }else{
            if(event.target.getAttribute("class") == "oculto blokeada"){
                event.target.classList.remove("blokeada");
                event.target.addEventListener("click", Buscaminas.cambiarEstadoDeCelda);
                document.getElementById("bombas").innerHTML = parseInt(document.getElementById("bombas").innerHTML) +1;
            }    
        }
    },   
       
    generarCampoDeMinas: function(){
        var eTabla = document.createElement("table"),
            eFila,eCelda;
        for (var i = 0; i < this.alto; i++){
            eFila =document.createElement("tr");
            for (var j =0; j < this.ancho; j++){
                eCelda = document.createElement("td");
                eCelda.setAttribute("id",i+''+j);
                eCelda.setAttribute("class","oculto");
                eFila.appendChild(eCelda);
            }
            eTabla.appendChild(eFila);
        }
        eTabla.setAttribute("class","campoDeMinas");
        return eTabla;
    
    },    
        
        
    generarPanelDeControl: function(){
        var ePanel = document.createElement("div");
        ePanel.appendChild(this.generarContadorMinas());
        ePanel.appendChild(this.generarBotonInicio());
        ePanel.appendChild(this.generarTemporizador());
        ePanel.setAttribute("class","panelDeControl");
        return ePanel;
    },
    
    generarTemporizador: function(){
        var eTempo = document.createElement("div"),
            eContador = document.createElement("p"),
            tContador = document.createTextNode("000");
        eContador.appendChild(tContador);
        eTempo.appendChild(eContador);
        eTempo.setAttribute("class","temporizador");
        eContador.setAttribute("id","segundos");
        return eTempo;
    },
    
    generarBotonInicio: function(){
        var eInicio = document.createElement("div"),
            eImagen = document.createElement("img");
        eImagen.setAttribute("src","imagenes/carita.jpeg");
        eInicio.appendChild(eImagen);
        eInicio.setAttribute("class","botonInicio");
        eInicio.addEventListener("click", function calliniciar(){Buscaminas.iniciarMatriz();this.removeEventListener("click", calliniciar)});
        return eInicio;
    },
    
    generarContadorMinas: function(){
        var eConteM = document.createElement("div"),
            eContadorM = document.createElement("p"),
            tContarM = document.createTextNode("00");
        eContadorM.appendChild(tContarM);
        eContadorM.setAttribute("id","bombas");
        eConteM.appendChild(eContadorM);
        eConteM.setAttribute("class", "contadorMinas");
        return eConteM;
    
    },
   ///////////////////////////////////////////INICIAR MATRIZ////////////////////////////////////////////////     
    
    verVecino: function(fila,columna){
        if( matriz[fila][columna] != -1){                             // verifica si es bomba
           matriz[fila][columna]++; 
        }    
    },
        
    sumarVecinos: function(fila,columna){
        if((fila>0 && fila<Buscaminas.alto-1) && (columna>0 && columna<Buscaminas.ancho-1)){ //CENTER
            Buscaminas.verVecino(fila,columna+1);
            Buscaminas.verVecino(fila,columna-1);
            Buscaminas.verVecino(fila+1,columna+1);
            Buscaminas.verVecino(fila+1,columna);
            Buscaminas.verVecino(fila+1,columna-1);
            Buscaminas.verVecino(fila-1,columna+1);
            Buscaminas.verVecino(fila-1,columna);
            Buscaminas.verVecino(fila-1,columna-1);
        }else if ((fila == 0) && (columna>0 && columna<Buscaminas.ancho-1)){  //TOP
                  Buscaminas.verVecino(fila,columna-1);
                  Buscaminas.verVecino(fila,columna+1);
                  Buscaminas.verVecino(fila+1,columna);
                  Buscaminas.verVecino(fila+1,columna-1);
                  Buscaminas.verVecino(fila+1,columna+1);
                  }else if ((fila == Buscaminas.alto-1) && (columna>0 && columna<Buscaminas.ancho-1)){ //BOTTOM
                  Buscaminas.verVecino(fila,columna-1);
                  Buscaminas.verVecino(fila,columna+1);
                  Buscaminas.verVecino(fila-1,columna);
                  Buscaminas.verVecino(fila-1,columna-1);
                  Buscaminas.verVecino(fila-1,columna+1);
                  }else if ((columna == 0) && (fila>0 && fila<Buscaminas.alto-1)){ //LEFT
                  Buscaminas.verVecino(fila-1,columna);
                  Buscaminas.verVecino(fila+1,columna);
                  Buscaminas.verVecino(fila+1,columna+1);
                  Buscaminas.verVecino(fila-1,columna+1);
                  Buscaminas.verVecino(fila,columna+1);
                  }else if ((columna == Buscaminas.ancho-1) && (fila>0 && fila<Buscaminas.alto-1)){ //RIGHT
                  Buscaminas.verVecino(fila-1,columna);
                  Buscaminas.verVecino(fila+1,columna);
                  Buscaminas.verVecino(fila+1,columna-1);
                  Buscaminas.verVecino(fila-1,columna-1);
                  Buscaminas.verVecino(fila,columna-1);
                  }else if ((columna == 0) && (fila == 0)){ //TOP-LEFT
                  Buscaminas.verVecino(fila+1,columna);
                  Buscaminas.verVecino(fila,columna+1);
                  Buscaminas.verVecino(fila+1,columna+1);
                  }else if ((columna == 0) && (fila == Buscaminas.alto-1)){ //TOP-RIGHT
                  Buscaminas.verVecino(fila-1,columna);
                  Buscaminas.verVecino(fila,columna+1);
                  Buscaminas.verVecino(fila-1,columna+1);
                  }else if ((columna == Buscaminas.ancho-1) && (fila == Buscaminas.alto-1)){ //BOTTOM-RIGHT
                  Buscaminas.verVecino(fila-1,columna);
                  Buscaminas.verVecino(fila,columna-1);
                  Buscaminas.verVecino(fila-1,columna-1);
                  }else if ((columna == Buscaminas.ancho-1) && (fila == 0)){ //BOTTOM-LEFT
                  Buscaminas.verVecino(fila+1,columna);
                  Buscaminas.verVecino(fila,columna-1);
                  Buscaminas.verVecino(fila+1,columna-1);
                  }     
    },    
        
    generarBombas: function(){
        var cantidadBombas = Math.floor(Buscaminas.alto*Buscaminas.ancho/cantBombas), b=0,x=0,y=0;
        while (b < cantidadBombas) {
          x = parseInt(Math.random() * Buscaminas.alto);
          y = parseInt(Math.random() * Buscaminas.ancho);
          if (matriz[x][y] != -1) {
            matriz[x][y] = -1;
            arrayBombas.push(x);
            arrayBombas.push(y); 
            b++;
          }
        }
        b=0;
        while( b <= arrayBombas.length){
            Buscaminas.sumarVecinos(arrayBombas[b],arrayBombas[b+1]);
            b= b+2;
        }
    },
        
        iniciarMatriz: function(){   // Inicio
        for (var i = 0; i < Buscaminas.alto; i++){
                matriz[i]= new Array();
            for (var j =0; j < Buscaminas.ancho; j++){
                matriz[i][j]=0;
                var celda = document.getElementById(i+''+j);
                celda.addEventListener("click", Buscaminas.cambiarEstadoDeCelda);
                celda.addEventListener("contextmenu",function(){Buscaminas.toogleBloqueada});
            }
        }    
            Buscaminas.generarBombas();
            document.getElementById("bombas").innerHTML= Math.floor(Buscaminas.alto*Buscaminas.ancho/cantBombas);
            Buscaminas.iniciarTiempo();
    },
//////////////////////////////////////////////PANEL DE CONTROL//////////////////////////////////////////////////////
     iniciarTiempo: function(){
         var contador_s =0, s = document.getElementById("segundos");
         cronometro = setInterval(
              function(){
                  if(contador_s > 99){
                  s.innerHTML = contador_s;
                      }else{
                          if(contador_s > 9){
                              s.innerHTML = '0'+contador_s;
                          }else{
                              s.innerHTML = '00'+contador_s;
                          } 
                      }
                  contador_s++;
                  if(contador_s == 999){
                        clearInterval(cronometro);
                    }
              }
              ,1000);
         
       },
        
        detenerTiempo: function(){
            clearInterval(cronometro);
        },
        
        verificarWin: function(n){
            if(n == (Buscaminas.alto*Buscaminas.ancho - Math.floor(Buscaminas.alto*Buscaminas.ancho/cantBombas))){
                var eFondo,eBoton,eTexto;
                Buscaminas.detenerTiempo();
                Buscaminas.includeCSS();
                eFondo=document.createElement("div");
                eBoton=document.createElement("div");
                eTexto=document.createElement("p");
                eFondo.setAttribute("class","winF");
                eBoton.setAttribute("class","winB");
                eBoton.addEventListener("click",function(){ location.reload(true);});
                eTexto.setAttribute("id","winT");
                eTexto.innerHTML='YOU WIN';
                eBoton.appendChild(eTexto);
                eFondo.appendChild(eBoton);
                document.getElementsByTagName("body")[0].appendChild(eFondo);
            }
        },
        
        includeCSS: function () {
            var link = document.createElement( "link" );
            link.href = "style/gameOver.css";
            link.rel = "stylesheet";
            document.getElementsByTagName("head")[0].appendChild( link );	
       },
}